// DESENVOLVIDO POR:
//     NATAN DE OLIVEIRA PEREIRA DA SILVA
// AT:
//     NATAN673@GMAIL.COM
// BITBUCKET:
//     https://bitbucket.org/nops674/profile/repositories?visibility=public

var backgroungImgUrlDados = "url('includes/img/backgroundDados.jpg')";
var backgroungImgUrlRecarga = "url('includes/img/backgroundRecarga.jpg')";

function flutuanteViewConstrutor(info, tipo) {
    var elementoFlutuante = $(".divFlutuante");

    elementoFlutuante.each(function() {
        $(this).find('.amount').text("")
        $(this).find('.bonus_amount').text("");
    })

    for (var i = 0; i < info.length; i++) {
        if (i > 0 && ($(".divFlutuante").length < 2)) {
            var elementoFlutuanteClone = $(".divFlutuante").clone();
            $(elementoFlutuanteClone).removeClass("par");
            $(".sectionPrincipal").append(elementoFlutuanteClone);            
        }

        var elementoFlutuante = $(".divFlutuante")[i];

        if (tipo == "recarga") {
            $(elementoFlutuante).find('a').attr('href', '#Recarga-' + info[i].amount);
            $(elementoFlutuante).find('.amount').text("R$" + info[i].amount);
            $(elementoFlutuante).find('.bonus_amount').text("E ganhe R$ " + info[i].bonus_amount + " de bônus");
        } else {
            $(elementoFlutuante).find('a').attr('href', '#Dados-' + info[i].gb_amount);
            $(elementoFlutuante).find('.amount').text(info[i].gb_amount + "GB");
            $(elementoFlutuante).find('.bonus_amount').text("Redes sociais ilimitadas");
        }

        if ($(elementoFlutuante).is(":hidden")) $(elementoFlutuante).show();
    }
}


function mudarView(params) {
    $("nav a.active").removeClass("active");

    if(params == "dados"){        
        $("nav a#dados").addClass("active");
        $(".sectionPrincipalBackground").attr('style', 'background-image: ' + backgroungImgUrlDados);
        flutuanteViewConstrutor(infoDados, 'dados');
    }else{
        $("nav a#recarga").addClass("active");
        $(".sectionPrincipalBackground").attr('style', 'background-image: ' + backgroungImgUrlRecarga);
        flutuanteViewConstrutor(infoRecarga, 'recarga');
    } 
}