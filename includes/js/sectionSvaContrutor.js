// DESENVOLVIDO POR:
// NATAN DE OLIVEIRA PEREIRA DA SILVA
// AT:
// NATAN673@GMAIL.COM
// BITBUCKET:
// https://bitbucket.org/nops674/profile/repositories?visibility=public


function svaViewConstrutor(info) {
    var elementoFlutuanteSva = $(".divFlutuanteSVA");

    elementoFlutuanteSva.each(function () {
        $(this).find('#amount').text("")
        $(this).find('#bonus_amount').text("");
    })

    for (var i = 0; i < info.length; i++) {
        if (i > 0) {
            var elementoFlutuanteSvaClone = $($(".divFlutuanteSVA")[0]).clone();
            //$(elementoFlutuanteSvaClone).removeClass("par");
            $(".sectionSVA").append(elementoFlutuanteSvaClone);
        }

        var elementoFlutuanteSva = $(".divFlutuanteSVA")[i];
        backgroungImgUrlFlutuanteSva = "url('" + info[i].image + "')"
        $(elementoFlutuanteSva).find('.backgroundSVA').attr('style', 'background-image: ' + backgroungImgUrlFlutuanteSva);
        $(elementoFlutuanteSva).find('.name').text(info[i].name);
        $(elementoFlutuanteSva).find('.gain').text(info[i].gain);
        $(elementoFlutuanteSva).find('.description').text(info[i].description);
        $(elementoFlutuanteSva).find('a').attr('href','#SVA-' + i);

        if ($(elementoFlutuanteSva).is(":hidden")) $(elementoFlutuanteSva).show();
    }
}