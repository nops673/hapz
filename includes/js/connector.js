// DESENVOLVIDO POR:
//     NATAN DE OLIVEIRA PEREIRA DA SILVA
// AT:
//     NATAN673@GMAIL.COM
// BITBUCKET:
//     https://bitbucket.org/nops674/profile/repositories?visibility=public



/*https://tidal-hearing.glitch.me/recarga para planos de recarga
https://tidal-hearing.glitch.me/dados para planos de dados 
https://tidal-hearing.glitch.me/sva para sva*/


var urlRecargaEndpoint = "https://tidal-hearing.glitch.me/recarga";
var urlDadosEndpoint = "https://tidal-hearing.glitch.me/dados";
var urlSvaEndpoint = "https://tidal-hearing.glitch.me/sva";


function obterInfoRecarga() {
    $.ajax({
        method: "GET",
        url: urlRecargaEndpoint,
        cache: false
    }).done(function (success) {
        infoRecarga = success;
        flutuanteViewConstrutor(infoRecarga, 'recarga');    
        return infoRecarga;

    }).fail(function (error) {
            errorLog(error);
       }
    );
}

function obterInfoDados() {
    $.ajax({
        method: "GET",
        url: urlDadosEndpoint,
        cache: false
    }).done(function (success) {
        return infoDados = success;
    }).fail(function (error) {
            errorLog(error);
        }
    );
}




function obterInfoSva() {
    $.ajax({
        method: "GET",
        url: urlSvaEndpoint,
        cache: false
    }).done(function (success) {
        infoSva = success;
        svaViewConstrutor(infoSva);
        return infoSva;
    }).fail(function (error) {
        errorLog(error);
    }
    );
}


function errorLog(error) {
    console.log("Algo deu errado na requisição!");
    console.log(error);
}

